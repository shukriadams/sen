(function(){

    klon.register("haku.views.accordion", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.accordion' }).text);
        },

        onShow : function (){
            $(document).foundation({});
        }

    }));

}());