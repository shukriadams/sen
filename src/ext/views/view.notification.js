(function(){

    klon.register("haku.views.notification",  haku.views.base.basic.type().extend({
        html : '<div> <hr /><div data-role="console"></div> </div>',

        onShow : function (){
            var self = this;

            // note, doesnt display on android
            window.plugin.notification.badge.set(10);

            haku.foreground = function (id, json) {
                //var s = JSON.stringify(json) + " id : " + id + " foreground";
                //self.write(s);
                //window.plugin.notification.local.cancel(id);
            };

            haku.background = function(id, json){
                var s = JSON.stringify(json) + " id : " + id  + " background";
                self.write(s);
                window.plugin.notification.local.cancel(id);
            };

            window.plugin.notification.local.add({
                id: "2",
                title : 'the title',
                message: 'This is a message body that is really long',
                json : { test : 123 },
                foreground: 'haku.foreground',
                background : 'haku.background'
            });


        },

        write : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.append("<div>" + message +"<hr /></div>");
        }
    }));

}());