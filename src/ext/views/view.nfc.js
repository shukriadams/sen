(function(){

    klon.register("haku.views.nfc", haku.views.base.basic.type().extend({
        showRawTagContent : false,
        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.nfc' }).text);
        },

        events : {
            "change #cbShowRaw" : "showRawTagContentChanged"
        },

        showRawTagContentChanged : function(){
            this.showRawTagContent = $('#cbShowRaw').is(':checked');
        },

        onShow : function(){
            var self = this;

            function failure(reason) {
                self.write(reason);
            }

            nfc.addNdefListener(
                function(nfcEvent){
                    self.onNdef(nfcEvent);
                },
                function() {
                    console.log("Listening for NDEF tags.");
                },
                failure
            );

            nfc.addTagDiscoveredListener(
                function(nfcEvent){
                    self.onNfc(nfcEvent);
                },
                function() {
                    console.log("Listening for non-NDEF tags.");
                },
                failure
            );

            // Android launches the app when tags with mime type text/pg are scanned
            // because of an intent in AndroidManifest.xml.
            // phonegap-nfc fires an ndef-mime event (as opposed to an ndef event)
            // the code reuses the same onNfc handler
            nfc.addMimeTypeListener(
                'text/pg',
                function(nfcEvent){
                    self.onNdef(nfcEvent);
                },
                function() {
                    console.log("Listening for NDEF mime tags with type text/pg.");
                },
                failure
            );
        },

        onNfc: function (nfcEvent) {
            var payload = this.getPayLoad(nfcEvent.tag);
            if (payload){
                this.write("Tag read, payload is " + payload);
            }
        },

        onNdef: function (nfcEvent) {
            var payload = this.getPayLoad(nfcEvent.tag);
            if (payload){
                this.write("Tag read, payload is " + payload);
            } else{
                this.write("Tag is empty");
            }

            if (this.showRawTagContent){
                this.write("Raw tag : <br />");
                var raw = JSON.stringify(nfcEvent.tag);
                raw = raw.replace(/,'/g, ", '");
                raw = raw.replace(/,"/g, ', "');
                this.write(raw);
            }

            var txtPush = this.$el.find("#txtPayload");
            var blankTag = this.$el.find('#cbWriteEmpty').is(':checked');

            if (blankTag){
                this.write("attempting to blank tag");
                this.eraseTag(nfcEvent);
            }
            else if (txtPush.val().length > 0){
                this.writeToTag(nfcEvent, txtPush.val() + payload);
                txtPush.val('');
            }

        },

        //clears tag
        eraseTag : function(){
            var self = this;
            nfc.erase(
                function () {
                    self.write("erased tag");
                },
                function (reason) {
                    self.write("error " + reason);
                }
            );

        },

        // writes payload to tag
        writeToTag : function(payload){
            var mimeType = "text/plain";
            var record = ndef.mimeMediaRecord(mimeType, nfc.stringToBytes(payload));
            var self = this;

            nfc.write(
                [record],
                function () {
                     self.write("wrote '" + payload + "' to tag");
                },
                function (reason) {
                    self.write("error " + reason);
                }
            );
        },

        //
        getPayLoad : function(tag){

            var result = "";
            if (tag.ndefMessage && tag.ndefMessage.length > 0){
                for(var i = 0 ; i < tag.ndefMessage.length ; i ++){
                    if (!tag.ndefMessage[i].payload )
                        continue;
                    var payload = nfc.bytesToString(tag.ndefMessage[i].payload);
                    if (payload){
                        result += payload + " | ";
                    }
                }
            }
            return result;
        },

        //
        write : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.append("<div>" + message +"<hr /></div>");
        }

    }));

}());