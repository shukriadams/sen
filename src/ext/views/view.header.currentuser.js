﻿(function(){

    klon.register("haku.views.header.currentUser", haku.views.base.basic.type().extend({
        
        // expects : options.authentication
        initialize: function () {

            var auth = haku.authentication;

           	// todo : move to render()
            if (auth .isAuthenticated) {
               this.html = auth .username;
            }
        }
    }));

}());