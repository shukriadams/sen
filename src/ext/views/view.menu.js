﻿(function(){

    klon.register("haku.views.sideMenus", Backbone.View.extend({

        render: function () {
            var self = this;
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.menu' }).text);
            this.$el.html(this.template());

            // bind route urls to page links, this should be moved to own view
            this.$el.find('[data-route]').click(function (e) {
                var target = $(e.target);
                var url = target.closest('[data-route]').data('route');
                haku.router.navigate(url, { trigger: true });
            });

            // automatically close menu when a link on it is closed. Might want to add special "close"
            this.$el.find("a").click(function(){
                $(".exit-off-canvas").click();
            });
        }

    }));

}());