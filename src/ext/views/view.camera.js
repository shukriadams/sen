(function(){

    klon.register("haku.views.camera", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.camera' }).text);
        },

        onShow : function(){

            var pictureSource;   // picture source
            var destinationType; // sets the format of returned value

            pictureSource = navigator.camera.PictureSourceType;
            destinationType = navigator.camera.DestinationType;

            this.$el.find("#btnCapturePhoto").click(function(){
                // Take picture using device camera and retrieve image as base64-encoded string
                navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
                    destinationType: destinationType.DATA_URL });
            });

            this.$el.find("#btnCapturePhotoEdit").click(function(){
                // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
                navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
                    destinationType: destinationType.DATA_URL });
            });

            /*
            document.addEventListener("deviceready", onDeviceReady, false);

            function onDeviceReady() {
                pictureSource=navigator.camera.PictureSourceType;
                destinationType=navigator.camera.DestinationType;
            }
            */
            function onPhotoDataSuccess(imageData) {
                // Uncomment to view the base64-encoded image data
                // console.log(imageData);

                // Get image handle
                //
                var smallImage = document.getElementById('smallImage');

                // Unhide image elements
                //
                smallImage.style.display = 'block';

                // Show the captured photo
                // The in-line CSS rules are used to resize the image
                //
                if (navigator.camera.shim)
                    smallImage.src = imageData;
                else
                    smallImage.src = "data:image/jpeg;base64," + imageData;
            }

            // Called when a photo is successfully retrieved
            //
            function onPhotoURISuccess(imageURI) {
                // Uncomment to view the image file URI
                // console.log(imageURI);

                // Get image handle
                //
                var largeImage = document.getElementById('largeImage');

                // Unhide image elements
                //
                largeImage.style.display = 'block';

                // Show the captured photo
                // The in-line CSS rules are used to resize the image
                //
                largeImage.src = imageURI;
            }

            // A button will call this function
            //
            function getPhoto(source) {
                // Retrieve image file location from specified source
                navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
                    destinationType: destinationType.FILE_URI,
                    sourceType: source });
            }

            // Called if something bad happens.
            //
            function onFail(message) {
                alert('Failed because: ' + message);
            }
        }

    }));

}());