(function(){

    klon.register("haku.views.barcode", haku.views.base.basic.type().extend({

        events : {
            "click #btnScan" : "scan"
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.barcode' }).text);
        },

        onShow : function(){

        },

        scan : function(){
            var self = this;
            var scanner = cordova.require("cordova/plugin/BarcodeScanner");
            scanner.scan( function (result) {

                self.write("We got a barcode\n" +
                    "Result: " + result.text + "\n" +
                    "Format: " + result.format + "\n" +
                    "Cancelled: " + result.cancelled);

                self.write("Scanner result: \n" +
                    "text: " + result.text + "\n" +
                    "format: " + result.format + "\n" +
                    "cancelled: " + result.cancelled + "\n");
                self.write(result.text);

                /*
                 if (args.format == "QR_CODE") {
                 window.plugins.childBrowser.showWebPage(args.text, { showLocationBar: false });
                 }
                 */

            }, function (error) {
                console.log("Scanning failed: ", error);
            } );

        },

        write : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.append("<div>" + message +"<hr /></div>");
        }

    }));

}());