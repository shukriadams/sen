﻿(function(){

    klon.register("haku.views.body", haku.views.base.basic.type().extend({

        initialize : function () {
            var self = this;

            haku.authentication.on("changed", function(){
                self.renderCurrentUser();
            });
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.body' }).text);
        },

        renderCurrentUser : function(){
            var currentuserheader = haku.views.header.currentUser.instance();

            currentuserheader.render();
            $('[data-layout-role="currentuser"]').html(currentuserheader.html);
        }

    }));

}());