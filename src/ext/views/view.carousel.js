(function(){

    // note : always set a height property on carousel (data-orbit), else it render collapsed
    klon.register("haku.views.carousel", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.carousel' }).text);
        },

        onShow : function (){
            klon.base(this, "render", arguments);
            $(document).foundation({
                orbit: {}
            });
        }

    }));

}());