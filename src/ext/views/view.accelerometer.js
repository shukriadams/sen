(function(){

    klon.register("haku.views.accelerometer",  haku.views.base.basic.type().extend({
        html : '<div> <hr /><div data-role="console"></div> </div>',

        onShow : function (){
            var self = this;

            var options = { frequency: 500 };

            var watchID = navigator.accelerometer.watchAcceleration(
                function(acceleration){
                    self.write('acc : ');
                    var s = 'X:' + acceleration.x + ' <br/> Y:' + acceleration.y + ' <br/> Z: ' + acceleration.z;
                    self.write(s);

                },
                function(){
                    alert('onError!');
                },
                options);
        },

        write : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.html("<div>" + message +"</div>");
        }
    }));

}());