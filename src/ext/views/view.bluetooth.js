(function(){

    var addressKey = "address";
    var heartRateServiceAssignedNumber = "180d";
    var batteryServiceAssignedNumber = "180f";
    var batteryLevelCharacteristicAssignedNumber = "2a19";

    klon.register("haku.views.bluetooth",  haku.views.base.basic.type().extend({

        status : "initializing", // initialize | scan | connect | discover

        html : '<div> <hr /><div data-role="console"></div> </div>',

        // address of the device to connect to
        address : "",

        servicesToRead: [],

        serviceToReadIndex : 0,

        hasConnected : false,

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.bluetooth' }).text);
        },

        //
        onShow : function (){
            var self = this;
            this.setStatus("Initializing");

            bluetoothle.disconnect(
                function(){
                    self.wrate("cleared at start");
                    self.close1();
                }, function(){
                    self.wrate("cleared at start error");
                    self.close1();
                }
            );


        },

        close1 : function(){
            var self = this;
            bluetoothle.close(
                function(){
                    self.wrate("closed at start");
                    self.initBl();
                },
                function(){
                    self.wrate("closed at start error");
                    self.initBl();
                }
            );
        },

        initBl : function(){
            var self = this;
            bluetoothle.initialize(
                function(obj){
                    self.setStatus("initialized");
                    if (obj.status == "initialized")
                    {
                        self.wrate("Bluetooth initialized successfully, starting scan for devices.");
                        self.scan();
                    }
                    else
                    {
                        self.setStatus("Initialization failed.");
                        self.wrate("Unexpected initialize status: " + obj.status);
                    }
                }, function(obj){
                    self.setStatus("Initialization error");
                    self.wrate("Initialize error: " + obj.error + " - " + obj.message);
                }
            );
        },

        // invoked after initialization
        scan : function(){
            var self = this;
            var scanning = false;

            var id = setTimeout(function(){

                if (scanning)
                    return;
                scanning = true;

                bluetoothle.startScan(
                    function(obj){
                        clearTimeout(id);
                        scanning= false;
                        self.scanning(obj);
                    },
                    function(obj){
                        scanning= false;
                        self.wrate("Start scan error: " + obj.error + " - " + obj.message);
                    }, []
                );
            }, 3000);
        },

        found : 0,

        //
        scanning : function (obj)
        {
            var self = this;

            if (obj.status === "scanResult")
            {



                this.wrate("found stuff ..");
                self.wrate("found device " + JSON.stringify(obj));

                found ++;
                if (found > 10){

                    bluetoothle.stopScan(
                        function(){
                            self.wrate("stopped scanning");

                            self.address = obj.address;
                            self.goConnect();
                        },
                        function(){
                            self.wrate("stopped scanning error!");
                        }
                    );
                }
            }
            else if (obj.status === "scanStarted")
            {
                this.wrate("Scan was started successfully, stopping in 10");
                this.wrate("found device " + (obj ? JSON.stringify(obj) : "--"));
            }
            else
            {
                this.wrate("Unexpected start scan status: " + obj.status);
            }
        },

        //
        goConnect : function(){
            var self = this;

            self.wrate("Connecting to: " + self.address);
            var paramsObj = {"address": self.address};

            setTimeout(function(){
                bluetoothle.connect(
                    function(obj){
                        self.connected(obj);
                    }, function(obj){
                        self.wrate("Connect error: " + obj.error + " - " + obj.message);
                    }, paramsObj
                );
            }, 1000);

        },

        //
        goReconnect : function(){
            var self = this;

            self.wrate("reconnecting to: " + self.address);

            bluetoothle.reconnect(
                function(obj){
                    self.connected(obj);
                }, function(obj){
                    self.wrate("Reconnect error: " + obj.error + " - " + obj.message);
                }
            );
        },

        //
        connected : function (obj) {
            var self = this;

            if (obj.status === "connected")
            {
                self.wrate("Connected to : " + obj.name + " - " + obj.address);
                self.hasConnected = true;
                self.wrate(JSON.stringify(obj));


                if (self.servicesToRead.length > 0){
                    self.readServices();
                    self.wrate("already found services, skipping discovery");
                } else {

                    self.wrate("start disciverug");

                    setTimeout(function(){
                        bluetoothle.discover(
                            function(connectedd){
                                self.onDiscovered2(connectedd);
                            },
                            function(discovererr){
                                self.wrate("Discover error: " + discovererr.error + " - " + discovererr.message);
                            }
                        );

                    }, 500);

                }
            }
            else if (obj.status == "connecting")
            {
                self.wrate("still Connecting to : " + obj.address);
                self.wrate(JSON.stringify(obj));
            }
            else
            {
                self.wrate("Unexpected connect status: " + obj.status);
                if (self.hasConnected )
                    self.goReconnect();
                else
                    self.goConnect();
            }

        },

        onDiscovered2 : function(conn){

            var self = this;

            if (conn.status === "discovered")
            {
                self.wrate("Discovery completed");
                // self.wrate(JSON.stringify(obj));
                var dump = "";

                if (conn.services){

                    for (var i = 0 ; i  < conn.services.length ; i ++){
                        var service = conn.services[i];

                        dump += "<div> svc : " + service.serviceAssignedNumber;


                        for (var j = 0 ; j < service.characteristics.length ; j ++){
                            var characteristic = service.characteristics[j];

                            dump += " c:" + characteristic.characteristicAssignedNumber;

                            /*
                            self.servicesToRead.push({
                                serviceAssignedNumber : service.serviceAssignedNumber,
                                characteristicAssignedNumber : characteristic.characteristicAssignedNumber
                            });
                            */

                            /*
                            WARNING THIS SEEMS OT BREAK STUFF
                            for (var k = 0 ; k < characteristic.descriptors.length ; k ++){
                                var descriptor = characteristic.descriptors[k];
                                dump += "|" + descriptor.descriptorAssignedNumber;
                            }
                            */
                        }

                        dump += "</div>";
                    }

                }

                self.wrate(dump);
                self.wrate("attempting to read " +  self.servicesToRead.length  + " services");
                //self.readServices();
            }
            else
            {
                self.wrate("Unexpected discover status: " + conn.status);
            }
        },

        readServices : function(){
            var self = this;

            while (self.serviceToReadIndex < self.servicesToRead.length){
                var service = self.servicesToRead[self.serviceToReadIndex];

                self.wrate("trying to read service " + service.serviceAssignedNumber + "::" + service.characteristicAssignedNumber);

                self.serviceToReadIndex ++;

                var paramsObj = {
                    "serviceAssignedNumber": "180f", // service.serviceAssignedNumber ,
                    "characteristicAssignedNumber": service.characteristicAssignedNumber
                };


                setTimeout(function(){
                    bluetoothle.read(
                        function(obj){
                            self.onRead(obj, service.serviceAssignedNumber);
                        },
                        function(obj){
                            self.wrate("Read error: " + obj.error + " - " + obj.message + " : " + JSON.stringify(obj));
                        },
                        paramsObj
                    );
                }, 1000); // wait a bit

            }

            if (self.serviceToReadIndex >= self.servicesToRead.length){
                self.wrate("done reading services!");
            }
        },

        onRead : function(obj, serviceAssignedNumber){
            var self = this;
            self.wrate("attempting to read byte from service " + serviceAssignedNumber);
            if (obj.status == "read")
            {
                var bytes = bluetoothle.getBytes(obj.value);
                var dump = "read serviceAssignedNumber : " + serviceAssignedNumber + " -";
                for (var i = i ; i < bytes.length ; i ++){
                    dump += bytes[i];
                }

                self.wrate(dump);
            }
            else
            {
                self.wrate("Unexpected read status: " + obj.status);
            }
        },

        wrate : function(message){
            //message = message.replace(/:"/g, ': "');
            var c = this.$el.find('[data-role="cconsole"]');
            c.append("<div>" + message + "<hr /></div>");
        },

        setStatus : function(status){
            // this.$el.find('#status').html(status);
        }

    }));

}());