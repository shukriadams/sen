(function(){

    klon.register("haku.views.gps",  haku.views.base.basic.type().extend({

        html : '<div> <hr /><div data-role="console"></div> </div>',

        onShow : function (){
            var self = this;

            navigator.geolocation.getCurrentPosition(onSuccess, onError);

            function onSuccess(position) {

                var s = 'Latitude: '       + position.coords.latitude              + '<br />' +
                    'Longitude: '          + position.coords.longitude             + '<br />' +
                    'Altitude: '           + position.coords.altitude              + '<br />' +
                    'Accuracy: '           + position.coords.accuracy              + '<br />' +
                    'Altitude Accuracy: '  + position.coords.altitudeAccuracy      + '<br />' +
                    'Heading: '            + position.coords.heading               + '<br />' +
                    'Speed: '              + position.coords.speed                 + '<br />' +
                    'Timestamp: '          + position.timestamp                    + '<br />';

                self.write(s);
            }

            function onError(error) {
                alert('code: '    + error.code    + '\n' +
                    'message: ' + error.message + '\n');
            }
        },

        write : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.append("<div>" + message +"<hr /></div>");
        }
    }));

}());