(function(){

    klon.register("haku.views.jsonLoader", haku.views.base.ajax.type().extend({
        url : 'http://localhost:8001/content/get.json',
        storeKey :"jsonloadertest",
        storeTimeout : 0,
        initialize : function(){
            this.html = "getting data ...";
        },
        onData : function(json){
            this.html = json;
        }
    }));

}());
