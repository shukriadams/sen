(function(){

    klon.register("haku.views.home", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.home' }).text);
        }

    }));

}());