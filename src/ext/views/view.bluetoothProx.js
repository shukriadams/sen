(function(){

    klon.register("haku.views.bluetoothProx",  haku.views.base.basic.type().extend({

        status : "initializing", // initialize | scan | connect | discover

        html : '<div> <hr /><div data-role="console"></div> </div>',

        beacons: [], // array of scanning objects

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.bluetoothProx' }).text);
        },

        //
        onShow : function (){
            var self = this;
            self.init();
        },

        init : function(){
            var self = this;
            bluetoothle.initialize(
                function(obj){
                    if (obj.status == "initialized")
                    {
                        self.scan();
                    }
                    else
                    {
                        self.writeToConsole("Unexpected initialize status: " + obj.status);
                    }
                }, function(obj){
                    self.setStatus("Initialization error");
                    self.writeToConsole("Initialize error: " + obj.error + " - " + obj.message);
                }
            );
        },

        // invoked after initialization
        scan : function(){
            var self = this;
            var scanning = false;

            var id = setTimeout(function(){

                if (scanning)
                    return;
                scanning = true;
                self.writeToConsole("started scan")
                bluetoothle.startScan(
                    function(obj){
                        clearTimeout(id);
                        scanning= false;
                        self.scanning(obj);
                    },
                    function(obj){
                        scanning= false;
                        self.writeToConsole("Start scan error: " + obj.error + " - " + obj.message);
                    }, []
                );
            }, 1000);
        },

        //
        scanning : function (obj)
        {
            var self = this;

            if (obj.status === "scanResult")
            {
                // find beacon in local array
                var beacon = null
                for (var i = 0 ; i < self.beacons.length ; i ++){
                    if (self.beacons[i].address === obj.address){
                        beacon = self.beacons[i];
                        break;
                    }
                }

                var now = Date.now();

                // create if not found
                if (beacon === null){
                    beacon = {
                        address : obj.address,
                        name : obj.name,
                        readings : [],
                        distance : null,    // rssi distance, int or null
                        lastRead : now      // date last read (in milliseconds from jan1 1970)
                    };
                    self.beacons.push(beacon);

                    var template = _.template('<div>{{ name }} ({{ address }}) : <span class="label" data-beacon-distance="{{ address }}"></span></div>');

                    this.$el.find('[data-role="beacons"]').append(template(beacon));
                }

                beacon.readings.push(Math.abs(obj.rssi));
                beacon.lastRead = now;
                var blockSize = 10;

                if (beacon.readings.length > blockSize){
                    var distance = 0;
                    for (var i = 0 ; i < beacon.readings.length ; i ++){
                        distance += beacon.readings[i];
                    }
                    distance = distance / beacon.readings.length;
                    distance = Math.floor(distance);

                    beacon.readings = [];
                    beacon.distance = distance;
                }

                // look for nonresponsvie beacons
                var millisecondsToExpire = 5000;
                for (var i = 0 ; i < self.beacons.length ; i ++){
                    if (self.beacons[i].distance !== null && now - self.beacons[i].lastRead > millisecondsToExpire){
                        self.beacons[i].distance = null;
                    }
                }

                // bind
                for (var i = 0 ; i < self.beacons.length ; i ++){
                    var b =self.beacons[i];
                    this.$el.find('[data-beacon-distance="' + b.address + '"]').html(b.distance? b.distance : "--");
                }
                /*
                bluetoothle.stopScan(
                    function(){
                        self.writeToConsole("stopped scanning");

                        self.address = obj.address;
                        //self.goConnect();
                    },
                    function(){
                        self.writeToConsole("stopped scanning error!");
                    }
                );
                */
            }
            else if (obj.status === "scanStarted")
            {
                //this.writeToConsole("Scan was started successfully, stopping in 10");
                //this.writeToConsole("found device " + (obj ? JSON.stringify(obj) : "--"));
            }
            else
            {
                this.writeToConsole("Unexpected start scan status: " + obj.status);
            }
        },

        writeToConsole : function(message){
            var c = this.$el.find('[data-role="console"]');
            c.append("<div>" + message + "<hr /></div>");
        }

    }));

}());