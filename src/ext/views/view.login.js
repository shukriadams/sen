(function(){

    klon.register("haku.views.login", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/views/view.login' }).text);
        },

        events: {
            "click #btnLogin": "processLogin"
        },

        processLogin: function () {
            var name = $("#txtName").val();
            var password = $("#txtPassword").val();
            haku.authentication.login(name, password);
        }

    }));

}());