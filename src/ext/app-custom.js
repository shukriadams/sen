// ===========================================================================
// override app
// ---------------------------------------------------------------------------
klon.register('haku.application', haku.application.type().extend({
    initialize : function(){

        klon.base(this, "initialize", arguments);

        // override underscore defaul template embed tags
        _.templateSettings = {
            evaluate: /\{%([\s\S]+?)%\}/g,
            interpolate: /\{\{([\s\S]+?)\}\}/g
        };

        require.config({
            baseUrl: haku.settings.systemPathRoot,
            paths: {
                'home': 'ext/views/view.home',
                'body': 'ext/views/view.body',
                'header.currentuser': 'ext/views/view.header.currentuser',
                'login': 'ext/views/view.login',
                'join': 'ext/views/view.join',
                'menu': 'ext/views/view.menu',
                'carousel': 'ext/views/view.carousel',
                'jsonloader' : 'ext/views/view.jsonLoader',
                'accordion' : 'ext/views/view.accordion',
                'camera' : 'ext/views/view.camera',
                'camera-shim' : 'shims/org.apache.cordova.camera',
                'nfc' : 'ext/views/view.nfc',
                'barcode' : 'ext/views/view.barcode',
                'notification' : 'ext/views/view.notification',
                'accelerometer' : 'ext/views/view.accelerometer',
                'bluetooth' : 'ext/views/view.bluetooth',
                'bluetoothProx' : 'ext/views/view.bluetoothProx',
                'gps' : 'ext/views/view.gps'
            },
            shim: {
                'body': { deps : ['header.currentuser'] },
                'camera' : { deps : ['camera-shim']}

            }
        });
    }
}));


// ===========================================================================
// override router
// ---------------------------------------------------------------------------
klon.register('haku.routers', haku.routers.type().extend({

    initialize : function(){
        klon.base(this, "initialize", arguments);
        var self = this;

        require(['body', 'menu'], function(){

            // render base body content
            var body = haku.views.body.instance();
            body.render();
            $('[data-layout-role="root"]').html(body.$el);
            // need to get attachment root from body once its rendered
            self.root = $('[data-layout-role="body"]');

            // render base left menu
            var menu = haku.views.sideMenus.instance();
            menu.render();
            $('[data-layout-role="mainmenu"]').html(menu.$el);

            self.home();

        });

    },

    routes: {
        "join": "join",
        "login": "login",
        "home" :  "home",
        "json" :  "json",
        "carousel" : "carousel",
        "accordion": "accordion",
        "camera": "camera",
        "nfc": "nfc",
        "barcode" : "barcode",
        "notification" : "notification",
        "accelerometer" : "accelerometer",
        "bluetooth": "bluetooth",
        "bluetoothProx": "bluetoothProx",
        "gps": "gps"
    },

    home : function(){
        var self = this;
        require(['home'], function(){
            var view = haku.views.home.instance();
            self._showPageView(view);
        });
    },

    json : function(){
        var self = this;
        require(['jsonloader'], function(){
            var view = haku.views.jsonLoader.instance();
            self._showPageView(view);
        });
    },

    join: function () {
        var self = this;
        require(['join'], function(){
            var view = haku.views.join.instance();
            self._showPageView(view);
        });
    },

    login: function () {
        var self = this;
        require(['login'], function(){
            var view = haku.views.login.instance();
            self._showPageView(view);
        });
    },

    carousel: function(){
        var self = this;
        require(['carousel'], function(){
            var view = haku.views.carousel.instance();
            self._showPageView(view);
        });
    },

    accordion: function(){
        var self = this;
        require(['accordion'], function(){
            var view = haku.views.accordion.instance();
            self._showPageView(view);
        });
    },

    camera: function(){
        var self = this;
        require(['camera'], function(){
            var view = haku.views.camera.instance();
            self._showPageView(view);
        });
    },

    nfc: function(){
        var self = this;
        require(['nfc'], function(){
            var view = haku.views.nfc.instance();
            self._showPageView(view);
        });
    },

    barcode : function(){
        var self = this;
        require(['barcode'], function(){
            var view = haku.views.barcode.instance();
            self._showPageView(view);
        });
    },

    notification : function(){
        var self = this;
        require(['notification'], function(){
            var view = haku.views.notification.instance();
            self._showPageView(view);
        });
    },

    accelerometer: function(){
        var self = this;
        require(['accelerometer'], function(){
            var view = haku.views.accelerometer.instance();
            self._showPageView(view);
        });
    },

    bluetooth: function(){
        var self = this;
        require(['bluetooth'], function(){
            var view = haku.views.bluetooth.instance();
            self._showPageView(view);
        });
    },

    bluetoothProx: function(){
        var self = this;
        require(['bluetoothProx'], function(){
            var view = haku.views.bluetoothProx.instance();
            self._showPageView(view);
        });
    },

    gps: function(){
        var self = this;
        require(['gps'], function(){
            var view = haku.views.gps.instance();
            self._showPageView(view);
        });
    }

}));

var app = haku.application.instance();
app.start();


